#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h> 

typedef struct{
  int id;
  int type;
  int target;
  char username[20];
  char content[512];
} mesgForm;

typedef struct{
  int id;
  char username[20];
}listUsers;

void print_menu();
void getPseudo(char * username);
void getListUser(mesgForm message, int sockfd);
void error(const char *msg);
void sendTo(mesgForm message, int sockfd);

int main(int argc, char *argv[])
{
  int sockfd, portno, n, quit=0;
  struct sockaddr_in serv_addr;
  struct hostent *server;
  int rep=0;
  mesgForm message;
  char username[20];
  char buffer[256];

  if (argc < 3) {
    fprintf(stderr,"usage %s hostname port\n", argv[0]);
    exit(0);
  }

  portno = atoi(argv[2]);
  sockfd = socket(AF_INET, SOCK_STREAM, 0);

  if (sockfd < 0) 
      error("ERROR opening socket");

  server = gethostbyname(argv[1]);

  if (server == NULL) {
      fprintf(stderr,"ERROR, no such host\n");
      exit(0);
  }

  bzero((char *) &serv_addr, sizeof(serv_addr));
  serv_addr.sin_family = AF_INET;

  bcopy((char *)server->h_addr, 
       (char *)&serv_addr.sin_addr.s_addr,
       server->h_length);

  serv_addr.sin_port = htons(portno);

  if (connect(sockfd,(struct sockaddr *) &serv_addr,sizeof(serv_addr)) < 0) 
      error("ERROR connecting");

  rep=0;

  bzero((char *) &message,sizeof(message));
  message.id=getpid();
  message.type=1;
  message.target=0;
  getPseudo(username);
  strcpy(message.username,username);
  n = write(sockfd,(char * ) &message,sizeof(message));
  if (n < 0) 
       error("ERROR writing to socket");

  printf("Bienvenu dans le tchat !\n");
  print_menu();

  do{
    fgets(message.content,512,stdin);
    strtok(message.content, "\n");  

    if(strcmp(message.content,"q")==0){
      message.type=2; 
      n = write(sockfd,(char * ) &message,sizeof(message));
      if (n < 0) 
        error("ERROR writing to socket");
      printf("Au revoir !\n");
      quit=1;
    }

    if(strcmp(message.content,"help")==0){
      print_menu();
    }

    if((strcmp(message.content,"list")==0)||(strcmp(message.content,"liste")==0)){
      getListUser(message,sockfd);
    }

    if(message.content[0]==47){
      sendTo(message, sockfd);
    }

    if((strcmp(message.content,"q")!=0)&&
      (strcmp(message.content,"list")!=0)&&
      (strcmp(message.content,"liste")!=0)&&
      (message.content[0]!=47)&&
      (strcmp(message.content,"help")!=0)){
      printf("Tu as taper n'imps.\n");
    }

  }while(1&&(quit==0));

  close(sockfd);
  return 0;
}

//Récupère la saisie du pseudo
void getPseudo(char * username)
{
  printf("Quel est votre pseudo ? \n");
  scanf("%s", username);
  getchar();
}

void error(const char *msg)
{
  perror(msg);
  exit(0);
}

void print_menu()
{
  printf("Voici les commandes disponibles : \n");
  printf("- Tapez q pour quitter \n");
  printf("- Taper liste pour afficher la liste des clients \n");
  printf("- Taper help pour afficher ce message\n");  
}

void getListUser(mesgForm message, int sockfd)
{
  // Pour récupéré la liste, on envoi l'odre au serveur
  // Le serveur nous retourne d'abord le nombres d'utilisateur
  // Le serveur nous retourne la liste
  // On parcour la liste en l'affichant
  int n, nbUser;
  listUsers users[20];

  // Je récupère le nombre d'utilisateur
  message.type=3;
  n = write(sockfd,(char * ) &message,sizeof(message));
  if (n < 0) 
       error("ERROR writing to socket");
  n = read(sockfd, &nbUser, sizeof(int));
  if (n < 0) 
       error("ERROR reading to socket");

  // Je récupère les utilisateurs
  message.type=4;
  n = write(sockfd,(char * ) &message,sizeof(message));
  if (n < 0) 
       error("ERROR writing to socket");

  n = read(sockfd, &users, sizeof(users));
  if (n < 0) 
       error("ERROR reading to socket");

  for (int i = 0; i < nbUser; ++i)
  {
    printf("pseudo : %s -- pid : %d\n", users[i].username, users[i].id);
  };

  bzero((char * ) &message, sizeof(message));
}

// Envoi un mp a un autre utilisateur en passant par le serveur.
void sendTo(mesgForm message, int sockfd)
{
  char content[512], *temp ;
  int code, n;

  //on découpe la chaine pour récupéré le pid et le message du client.
  temp =strchr(message.content,'/');
  temp++;
  code = atoi(temp);

  printf("Message a envoyer : ");
  fgets(content,512,stdin);
  strtok(content, "\n");

  message.type=5;
  message.target=code;
  strcpy(message.content,content);

  n = write(sockfd, (char * ) &message, sizeof(message));
  if (n < 0) 
       error("ERROR writing to socket");

  bzero((char * ) &message, sizeof(message));
}