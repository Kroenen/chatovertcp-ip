/* A simple server in the internet domain using TCP
   The port number is passed as an argument */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/shm.h>
#define CLEF_PRIRAGE_LIST			 0x00012349

typedef struct{
	int id;
	int type;
  int target;
	char username[20];
	char content[512];
} mesgForm;

typedef struct{
	int id;
	char username[20];
}listUsers;


int authentification(mesgForm message, int nbUser, int zone_reponse1, void * mem_partagee1);
int disconnect(mesgForm message, int nbUser, int zone_reponse1, void * mem_partagee1);
void error(const char *msg);
void sendNbUser(mesgForm message, int nbUser, int newsockfd);
void sendUsers(mesgForm message, int nbUser, int zone_reponse1, void * mem_partagee1, int newsockfd);
void sendTo(mesgForm message);

int main(int argc, char *argv[])
{
	int sockfd, newsockfd, portno;
	socklen_t clilen;
	char buffer[256];
	struct sockaddr_in serv_addr, cli_addr;
	int n;
	mesgForm message;
	int zone_reponse1;
	void *mem_partagee1;
	int nbUser = 0;

  zone_reponse1 =  shmget(CLEF_PRIRAGE_LIST,sizeof(listUsers),0700 | IPC_CREAT);
  if (zone_reponse1 == -1) { printf("Error : zone reponse\n"); }

	if((mem_partagee1 = shmat(zone_reponse1,NULL,0)) == (void*)-1){
  	printf("erreur de lecture \n");
  }

	if (argc < 2) {
		fprintf(stderr,"ERROR, no port provided\n");
		exit(1);
	}

	//On défini le socket
	sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if (sockfd < 0) 
		error("ERROR opening socket");
	
	//initialise les buffer a 0
	bzero((char *) &serv_addr, sizeof(serv_addr));

	//Vérifie si le port est un Numero et le stock
	portno = atoi(argv[1]);

	serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr.s_addr = INADDR_ANY;
	serv_addr.sin_port = htons(portno);

	//Attache une socket a une adresse
	if (bind(sockfd, (struct sockaddr *) &serv_addr,
		sizeof(serv_addr)) < 0) 
		error("ERROR on binding");

	//Met le process en attende
	listen(sockfd,5);
	clilen = sizeof(cli_addr);

	// Accept un connexion sur le socket sockfd
	newsockfd = accept(sockfd, 
		(struct sockaddr *) &cli_addr, 
		&clilen);

	if (newsockfd < 0) 
		error("ERROR on accept");

	do{
		bzero((char*) &message,sizeof(message));
		//Attend un message
		n = read(newsockfd,(char *) &message,sizeof(message));
		if (n < 0) error("ERROR reading from socket");

		switch(message.type){
			case 1: nbUser = authentification(message, nbUser, zone_reponse1, mem_partagee1);
							break;
			case 2: nbUser = disconnect(message, nbUser, zone_reponse1, mem_partagee1);
							break;
			case 3: sendNbUser(message, nbUser, newsockfd);
							break;
			case 4: sendUsers(message, nbUser, zone_reponse1, mem_partagee1, newsockfd);
							break;
			case 5: sendTo(message);
							break;
			default:break;
		}
	}
	while (1);

	close(newsockfd);
	close(sockfd);
	return 0;
}

void error(const char *msg)
{
    perror(msg);
    exit(1);
}

int authentification(mesgForm message, int nbUser, int zone_reponse1, void * mem_partagee1)
{
	listUsers users;

	printf("[AUTHENTIFICATION] : %s a envoyer un formulaire .\n", message.username);

	users.id = message.id;
	strcpy(users.username,message.username);

  ((listUsers*)mem_partagee1)[nbUser] = users;

  nbUser = nbUser+1;

  return nbUser;
}

int disconnect(mesgForm message, int nbUser, int zone_reponse1, void * mem_partagee1)
{
	listUsers users[20];
	int decalage = 0;

	printf("[DECONECTION] : %s a envoyer un formulaire .\n", message.username);

	for (int i = 0; i < nbUser; ++i)
	{
		users[i] = ((listUsers*)mem_partagee1)[i+decalage]; 
	  if(users[i].id==message.id){
	  	users[i] = ((listUsers*)mem_partagee1)[i+1]; 	
	  	decalage++;
	  	nbUser--;
  	};
  }

  // Une fois l'utilisateur supprimer de la liste, j'enregistre.
  for (int i = 0; i < nbUser; ++i)
  {
  	((listUsers*)mem_partagee1)[i]=users[i];
  }
	  
	return nbUser;
}

void sendNbUser(mesgForm message, int nbUser, int newsockfd)
{
	int n;

	printf("[getNbUser] : j'envoi le nombres d'utilisateur(%d) a %s \n",nbUser, message.username);

	n = write(newsockfd, &nbUser, sizeof(nbUser));
  if (n < 0) 
       error("ERROR writing to socket");
}

void sendUsers(mesgForm message, int nbUser, int zone_reponse1, void * mem_partagee1, int newsockfd)
{
	int n;
  listUsers users[20];

  for (int i = 0; i < nbUser; ++i)
  {
    users[i] = ((listUsers*)mem_partagee1)[i]; 
  }

  n = write(newsockfd, &users, sizeof(users));
  if (n < 0) 
       error("ERROR writing to socket");
}

void sendTo(mesgForm message)
{
	printf("%s veux envoyer %s a %d\n",message.username, message.content, message.target );
}